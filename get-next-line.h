/*
** getnextline.h for getnextline.h in /home/chabot_t/rendu/get_next_line/include
**
** Made by Thomas CHABOT
** Login   <chabot_t@epitech.net>
**
** Started on  Sun Dec 27 18:55:26 2015 Thomas CHABOT
** Last update Sun Apr 24 07:25:10 2016 Thomas CHABOT
*/

#ifndef	GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#ifndef READ_SIZE
# define READ_SIZE (4096)
#endif /* !READ_SIZE */

typedef struct	s_box
{
    int		nb_to_add;
    char		*line;
    char		*save;
    int		dep;
}		t_box;

typedef struct	s_sta
{
    int		nb_read;
    int		pos;
}		t_sta;

int		saveIt(t_box *);
int		finishIt(t_box, t_sta *, char *);
int		checkIt(t_box *, t_sta *, char *);
char	*getNextLine(int);
int     myStrLength(char *);

#endif /* GET_NEXT_LINE_H_ */
