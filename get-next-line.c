#include "get-next-line.h"

/**
 *
 * @param c
 */
void myPutChar(char c){
    write(1, &c, 1);
}

/**
 *
 * @param str
 */
void myPutStr(char* str){
    int i = 0;

    while (str[i]){
        myPutChar(str[i++]);
    }
}

/**
 *
 * @param str
 * @return
 */
int myStrLength(char* str){
    int i = 0;

    while (str[i]){
        i += 1;
    }
    return i;
}

int saveIt(t_box *dat)
{
    int i;
    int rest;

    dat->line == NULL ? rest = 0 : (rest = myStrLength(dat->line));
    if (!(dat->save = malloc(sizeof(dat->save) * (rest + dat->nb_to_add + 1))))
        return (-1);
    i = 0;
    while (rest-- > 0 && dat->line[i] != '\0')
    {
        dat->save[i] = dat->line[i];
        i++;
    }
    dat->save[i] = '\0';
    free(dat->line);
    dat->line = dat->save;
    return (0);
}

/**
 *
 * @param dat
 * @param tic
 * @param buffer
 * @return
 */
int	finishIt(t_box dat, t_sta *tic, char *buffer)
{
    int i;
    char *suit;
    char *new_oct;

    dat.line == NULL ? i = 0 : (i = myStrLength(dat.line));
    if (!(suit = malloc(sizeof(*suit) * (i + dat.nb_to_add + 1))))
        return (-1);
    new_oct = buffer + tic->pos;
    suit = dat.save + i;
    i = 0;
    while (dat.nb_to_add-- > 0 && new_oct[i] != '\0') {
        suit[i] = new_oct[i];
        i++;
    }
    suit[i] = '\0';
    return (0);
}

/**
 *
 * @param dat
 * @param tic
 * @param buffer
 * @return
 */
int checkIt(t_box *dat, t_sta *tic, char *buffer) {
    if ((tic->pos + 1) + dat->nb_to_add == tic->nb_read) {
        dat->nb_to_add += 1;
        if (saveIt(dat) == -1)
            return (-1);
        if (finishIt(*dat, tic, buffer) == -1)
            return (-1);
        tic->pos += dat->nb_to_add + 1;
    }
    dat->nb_to_add += 1;
    tic->nb_read <= tic->pos ? dat->dep = 1 : (dat->dep = 0);
    if (dat->dep == 1) {
        tic->pos = 0;
        dat->nb_to_add = 0;
    }
    return (0);
}

/**
 *
 * @param fd
 * @return
 */
char *getNextLine(const int fd)
{
    t_box dat;
    static t_sta tic;
    static char	buffer[READ_SIZE];

    dat.nb_to_add = 0;
    dat.line = NULL;
    while (buffer[tic.pos + dat.nb_to_add] != '\n') {
        if ((checkIt(&dat, &tic, buffer)) == -1)
            return (NULL);
        if (dat.dep == 1) {
            if ((tic.nb_read = read(fd, buffer, READ_SIZE)) < 0)
                return (NULL);
            if (tic.nb_read == 0)
                return (dat.line);
        }
    }
    if (saveIt(&dat) == -1)
        return (NULL);
    if (finishIt(dat, &tic, buffer) == -1)
        return (NULL);
    tic.pos += dat.nb_to_add + 1;
    return (dat.line);
}

void main(){
    char *s;
    int i = 0;

    while ((s = getNextLine(0))) {
        myPutStr(s);
        myPutChar('\n');
        free(s);
    }
}
